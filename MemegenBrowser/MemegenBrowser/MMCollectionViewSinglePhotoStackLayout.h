@protocol UICollectionViewDelegateMMSinglePhotoStackLayout <UICollectionViewDelegate>
@optional

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
@end

@interface MMCollectionViewSinglePhotoStackLayout : UICollectionViewLayout

@end
