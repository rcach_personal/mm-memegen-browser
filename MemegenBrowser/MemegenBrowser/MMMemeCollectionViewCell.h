@interface MMMemeCollectionViewCell : UICollectionViewCell

- (void)setImage:(UIImage *)image;
- (void)setAuthor:(NSString *)author;

@end
