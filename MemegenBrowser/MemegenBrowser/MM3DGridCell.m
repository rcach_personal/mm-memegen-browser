//
//  MM3DGridCell.m
//  MemegenBrowser
//
//  Created by Rene Cacheaux on 1/16/13.
//  Copyright (c) 2013 Rene Cacheaux. All rights reserved.
//

#import "MM3DGridCell.h"

@implementation MM3DGridCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
      self.layer.backgroundColor = [UIColor redColor].CGColor;
      self.layer.borderColor = [UIColor whiteColor].CGColor;
      self.layer.borderWidth = 2.0f;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
