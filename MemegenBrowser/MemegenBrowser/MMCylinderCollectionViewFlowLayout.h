//
//  MMCylinderCollectionViewFlowLayout.h
//  MemegenBrowser
//
//  Created by Rene Cacheaux on 1/13/13.
//  Copyright (c) 2013 Rene Cacheaux. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMCylinderCollectionViewFlowLayout : UICollectionViewFlowLayout

@end
