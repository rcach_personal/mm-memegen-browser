#import "MMMemeSlideshowViewController.h"

#import <Parse/Parse.h>

#import "MMCollectionViewSinglePhotoStackLayout.h"
#import "MMMemeCollectionViewCell.h"

static NSString * const kMemeCellReuseIdentifier = @"MemeCell";

@interface MMMemeSlideshowViewController ()<UICollectionViewDataSource,UICollectionViewDelegateMMSinglePhotoStackLayout>
@property(nonatomic, readonly) UICollectionView *collectionView;
@property(nonatomic, strong) NSMutableArray *photos;
@property(nonatomic, strong) NSTimer *slideShowTimer;
@property(nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property(nonatomic, strong) NSMutableArray *allMemes;
@property(nonatomic, assign) NSUInteger memeOffset;
@property(nonatomic, strong) NSMutableArray *displayedIndices;
@property(nonatomic, strong) NSDate *lastUpdateDate;
@property(nonatomic, strong) NSMutableArray *allMemeObjects;
@property(nonatomic, strong) NSMutableArray *photoObjects;
@property(nonatomic, strong) NSMutableArray *allLikes;
@property(nonatomic, strong) NSMutableArray *photoLikes;
@end

@implementation MMMemeSlideshowViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
      
    }
    return self;
}

#pragma mark - View Controller Lifecycle

- (void)loadView {
  self.view = [self newCollectionView];
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.memeOffset = 1;
  self.allLikes = [NSMutableArray array];
  self.photoLikes = [NSMutableArray array];
  self.allMemeObjects = [NSMutableArray array];
  self.photoObjects = [NSMutableArray array];
  self.allMemes = [NSMutableArray array];
  self.photos = [self newPhotosArray];
  self.displayedIndices = [NSMutableArray array];
  self.tapGestureRecognizer =
      [[UITapGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(handleTap:)];
  [self.view addGestureRecognizer:self.tapGestureRecognizer];
  
  UIImage *memegenSplashImage = [UIImage imageNamed:@"Default-Landscape"];
  UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:memegenSplashImage];
  backgroundImageView.frame = self.collectionView.bounds;
  backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
  backgroundImageView.autoresizingMask = (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
  self.collectionView.backgroundView = backgroundImageView;
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [self loadImages];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  
}

- (void)viewDidDisappear:(BOOL)animated {
  [super viewDidDisappear:animated];
  [self stopSlideShow];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
  return [self.photos count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  MMMemeCollectionViewCell *memeCell =
      [self.collectionView dequeueReusableCellWithReuseIdentifier:kMemeCellReuseIdentifier
                                                     forIndexPath:indexPath];
  [memeCell setImage:[self.photos objectAtIndex:indexPath.item]];
  PFObject *photoObject = [self.photoObjects objectAtIndex:indexPath.item];
  PFUser *user = [photoObject objectForKey:@"user"];
  NSString *authorName = [[user valueForKey:@"username"] stringByReplacingOccurrencesOfString:@"@mutualmobile.com" withString:@""];
//  authorName = [NSString stringWithFormat:@"%@", [self.photoLikes objectAtIndex:indexPath.item]];
  [memeCell setAuthor:authorName];

  return memeCell;
}

#pragma mark - UICollectionView Delegate MMSinglePhotoStackLayout 

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  UIImage *memeImage = [self.photos objectAtIndex:indexPath.item];
  
  CGFloat aspectRatio = memeImage.size.height / memeImage.size.width;
  CGFloat bigHeight = aspectRatio * self.view.bounds.size.width;
  CGFloat bigWidth = self.view.bounds.size.width;
  if (bigHeight > self.view.bounds.size.height) {
    aspectRatio = memeImage.size.width / memeImage.size.height;
    bigWidth = aspectRatio * self.view.bounds.size.height;
    bigHeight = self.view.bounds.size.height;
  }
  return CGSizeMake(bigWidth * 0.7f, (bigHeight * 0.7f) + 60.0f);
}

#pragma mark - Parse

- (void)lookForNewMemes {
  PFQuery *query = [PFQuery queryWithClassName:@"UserPhoto"];
  [query whereKey:@"createdAt" greaterThan:self.lastUpdateDate];
  [query orderByAscending:@"createdAt"];
  self.lastUpdateDate = [NSDate date];
  [query findObjectsInBackgroundWithBlock:^(NSArray *allObjects,
                                            NSError *error) {
    NSMutableArray *newMemes = [NSMutableArray array];
    NSMutableArray *newMemeObjects = [NSMutableArray array];
    NSMutableArray *newLikes = [NSMutableArray array];
    for (PFObject *object in allObjects) {
      PFFile *imageFile = [object objectForKey:@"imageFile"];
      NSData *imageData = [imageFile getData];
      UIImage *image = [UIImage imageWithData:imageData];
      if (image) {
        PFUser *user = [object objectForKey:@"user"];
        [user fetchIfNeeded];
        PFQuery *query = [PFQuery queryWithClassName:@"Activity"];
        [query whereKey:@"photo" equalTo:object];
        [query whereKey:@"type" equalTo:@"like"];
        
        [newLikes addObject:@([query countObjects])];
        [newMemes addObject:image];
        [newMemeObjects addObject:object];
      }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
      if ([newMemes count] > 0) {
        if ([self.allMemes count] == [self.displayedIndices count]) {
          self.memeOffset = [self.allMemes count];
        }
        [self.allMemes addObjectsFromArray:newMemes];
        [self.allMemeObjects addObjectsFromArray:newMemeObjects];
        [self.allLikes addObjectsFromArray:newLikes];
      }
    });
  }];

}

- (void)loadImages {
  NSMutableArray *allMemes = [NSMutableArray array];
  PFQuery *query = [PFQuery queryWithClassName:@"UserPhoto"];
  [query orderByAscending:@"createdAt"];
  query.limit = 1000;
  self.lastUpdateDate = [NSDate date];
  [query findObjectsInBackgroundWithBlock:^(NSArray *allObjects,
                                            NSError *error) {
    for (PFObject *object in allObjects) {
      
      PFFile *imageFile = [object objectForKey:@"imageFile"];
      NSData *imageData = [imageFile getData];
      UIImage *image = [UIImage imageWithData:imageData];
      if (image) {
        PFUser *user = [object objectForKey:@"user"];
        [user fetchIfNeeded];
        
        PFQuery *query = [PFQuery queryWithClassName:@"Activity"];
        [query whereKey:@"photo" equalTo:object];
        [query whereKey:@"type" equalTo:@"like"];
        
        [allMemes addObject:image];
        [self.allMemeObjects addObject:object];
        [self.allLikes addObject:@([query countObjects])];
      }
    }
    self.allMemes = allMemes;
    dispatch_async(dispatch_get_main_queue(), ^{
      [self startSlideShow];
    });
  }];
}

#pragma mark - SlideShow

- (void)startSlideShow {  
  [self.collectionView performBatchUpdates:^{
    [self.photos addObject:[self.allMemes objectAtIndex:0]];
    [self.photoObjects addObject:[self.allMemeObjects objectAtIndex:0]];
    [self.photoLikes addObject:[self.allLikes objectAtIndex:0]];
    [self.displayedIndices addObject:@(0)];
    [self.collectionView insertItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:0]]];
  } completion:nil];
  
  self.slideShowTimer =
    [NSTimer scheduledTimerWithTimeInterval:5
                                     target:self
                                   selector:@selector(showNextPicture)
                                   userInfo:nil
                                    repeats:YES];
}

- (void)stopSlideShow {
  if (self.slideShowTimer) {
    [self.slideShowTimer invalidate];
    self.slideShowTimer = nil;
  }
}

- (void)showNextPicture {
  if (self.memeOffset == [self.allMemes count]) {
    self.memeOffset = 0;
  }
  
  self.tapGestureRecognizer.enabled = NO;
  
  NSIndexPath *firstItemIndexPath =
      [NSIndexPath indexPathForItem:0 inSection:0];
//   UICollectionViewCell *cellToDelete = [self.collectionView cellForItemAtIndexPath:firstItemIndexPath];
  
//  [CATransaction setCompletionBlock:^{
  
    [self.collectionView performBatchUpdates:^{
      // Update model.
      
      
      if ([self.photos count] >= 5) {
        NSIndexPath *deletedItemIndexPath = [NSIndexPath indexPathForItem:[self.photos count] - 1 inSection:0];
        [self.photos removeLastObject];
        [self.photoObjects removeLastObject];
        [self.photoLikes removeLastObject];
        [self.collectionView deleteItemsAtIndexPaths:@[deletedItemIndexPath]];
      }
      
      [self.photos insertObject:[self.allMemes objectAtIndex:self.memeOffset] atIndex:0];
      [self.photoObjects insertObject:[self.allMemeObjects objectAtIndex:self.memeOffset] atIndex:0];
      [self.photoLikes insertObject:[self.allLikes objectAtIndex:self.memeOffset] atIndex:0];
      [self.collectionView insertItemsAtIndexPaths:@[firstItemIndexPath]];
      if (![self.displayedIndices containsObject:@(self.memeOffset)]) {
        [self.displayedIndices addObject:@(self.memeOffset)];
      }
      self.memeOffset++;
     
      
    }completion:^(BOOL finished){
      self.tapGestureRecognizer.enabled = YES;
    }];
//  }];
  
  // Insert custom animations.
//  cellToDelete.layer.opacity = 0.0f;
//  CAKeyframeAnimation *opacityAnimation = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
//  opacityAnimation.values = @[@(1.0f),@(1.0f),@(0.0f)];
//  opacityAnimation.keyTimes = @[@(0.0f),@(0.2f),@(1.0f)];
  
  // TODO: Place this somewhere the layout can have access.
//  CGPoint lowerRightEndPoint = CGPointMake(CGRectGetMidX(self.collectionView.bounds) + 40.0f,
//                                           CGRectGetMaxY(self.collectionView.bounds) * 1.5f);
//  
//  UIBezierPath *positionPath = [UIBezierPath bezierPath];
//  [positionPath moveToPoint:cellToDelete.center];
//  [positionPath addLineToPoint:lowerRightEndPoint];
//  
//  cellToDelete.layer.position = lowerRightEndPoint;
//  CAKeyframeAnimation *positionAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
//  positionAnimation.keyTimes = @[@(0.0f),@(1.0f)];
//  positionAnimation.path = positionPath.CGPath;
//  
//  CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
//  rotationAnimation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeRotation(180.0f * M_PI / 180, 0.0f, 0.0f, 1.0f)];
//
//  CAAnimationGroup *fallingImageAnimationGroup = [CAAnimationGroup animation];
//  fallingImageAnimationGroup.animations = @[positionAnimation, opacityAnimation, rotationAnimation];
//  fallingImageAnimationGroup.duration = 3.0;
//  fallingImageAnimationGroup.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
//  [cellToDelete.layer addAnimation:fallingImageAnimationGroup forKey:@""];
}

#pragma mark - Touch Handling

- (void)handleTap:(UITapGestureRecognizer *)tapGestureRecognizer {
  [self showNextPicture];
}

#pragma mark - Factory Methods

- (UICollectionView *)newCollectionView {
  MMCollectionViewSinglePhotoStackLayout *singlePhotoStackLayout =
      [[MMCollectionViewSinglePhotoStackLayout alloc] init];
  UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero
                                                        collectionViewLayout:singlePhotoStackLayout];
  collectionView.dataSource = self;
  collectionView.delegate = self;
  collectionView.backgroundColor = [UIColor lightGrayColor];
  [collectionView registerClass:[MMMemeCollectionViewCell class]
     forCellWithReuseIdentifier:kMemeCellReuseIdentifier];
  return collectionView;
}

- (NSMutableArray *)newPhotosArray {
  NSMutableArray *photos = [NSMutableArray array];
  return photos;
}

#pragma mark - Property Accessors

- (UICollectionView *)collectionView {
  return (UICollectionView *)self.view;
}

@end
