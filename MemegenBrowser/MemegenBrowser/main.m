//
//  main.m
//  MemegenBrowser
//
//  Created by Rene Cacheaux on 1/11/13.
//  Copyright (c) 2013 Rene Cacheaux. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MMAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([MMAppDelegate class]));
  }
}
