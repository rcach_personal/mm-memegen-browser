//
//  MMViewController.m
//  MemegenBrowser
//
//  Created by Rene Cacheaux on 1/11/13.
//  Copyright (c) 2013 Rene Cacheaux. All rights reserved.
//

#import "MMViewController.h"

@interface MMViewController ()
@property(nonatomic, strong) CALayer *layer0;
@property(nonatomic, strong) CALayer *layer1;
@property(nonatomic, strong) CALayer *layer2;
@property(nonatomic, strong) CALayer *layer3;
@property(nonatomic, strong) CALayer *layer4;
@property(nonatomic, strong) CALayer *testLayer;
@end

CGFloat radius = 700.0f;

@implementation MMViewController

CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;};
//CGFloat RadiansToDegrees(CGFloat radians) {return radians * 180 / M_PI;};

- (void)loadView {
  self.view = [[UIView alloc] init];
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  // Add perspective.
  CATransform3D perspectiveTransform = CATransform3DIdentity;
  perspectiveTransform.m34 = -1.0f/800.0f;
  self.view.layer.sublayerTransform = perspectiveTransform;
  
  
  
  CGFloat startingAngle = 10.0f;
  CGFloat numberOfLayers = 5;
  
  CGFloat angleStep = (180.0f - (startingAngle*2)) / (numberOfLayers - 1);
  
  CGFloat layerWidth = 480.0f;
  CGFloat layerHeight = 400.0f;
  
  UIColor *borderColor = [UIColor whiteColor];
  CGFloat borderWidth = 2.0f;
  
  
  CGFloat layer0Angle = startingAngle;
  self.layer0 = [CALayer layer];
  self.layer0.bounds = CGRectMake(0.0f, 0.0f, layerWidth, layerHeight);
  self.layer0.backgroundColor = [UIColor blueColor].CGColor;
  self.layer0.borderColor = borderColor.CGColor;
  self.layer0.borderWidth = borderWidth;
  self.layer0.zPosition = -1 * (sinf(DegreesToRadians(layer0Angle))*radius);
  self.layer0.transform = CATransform3DMakeRotation(DegreesToRadians(90 - startingAngle), 0.0f, 1.0f, 0.0f);
  
  CGFloat layer1Angle = startingAngle + angleStep;
  self.layer1 = [CALayer layer];
  self.layer1.bounds = CGRectMake(0.0f, 0.0f, layerWidth, layerHeight);
  self.layer1.backgroundColor = [UIColor greenColor].CGColor;
  self.layer1.borderColor = borderColor.CGColor;
  self.layer1.borderWidth = borderWidth;
  self.layer1.zPosition = -1 * (sinf(DegreesToRadians(layer1Angle))*radius);
  self.layer1.transform = CATransform3DMakeRotation(DegreesToRadians(90 - (startingAngle + angleStep)), 0.0f, 1.0f, 0.0f);
  
  self.layer2 = [CALayer layer];
  self.layer2.bounds = CGRectMake(0.0f, 0.0f, layerWidth, layerHeight);
  self.layer2.backgroundColor = [UIColor redColor].CGColor;
  self.layer2.borderColor = borderColor.CGColor;
  self.layer2.borderWidth = borderWidth;
//  self.layer2.zPosition = radius * -1;
  
  self.layer3 = [CALayer layer];
  self.layer3.bounds = CGRectMake(0.0f, 0.0f, layerWidth, layerHeight);
  self.layer3.backgroundColor = [UIColor yellowColor].CGColor;
  self.layer3.borderColor = borderColor.CGColor;
  self.layer3.borderWidth = borderWidth;
  self.layer3.zPosition = -1 * (sinf(DegreesToRadians(layer1Angle))*radius);
  self.layer3.transform = CATransform3DMakeRotation(DegreesToRadians(90 - (startingAngle + angleStep)), 0.0f, -1.0f, 0.0f);
  
  self.layer4 = [CALayer layer];
  self.layer4.bounds = CGRectMake(0.0f, 0.0f, layerWidth, layerHeight);
  self.layer4.backgroundColor = [UIColor orangeColor].CGColor;
  self.layer4.borderColor = borderColor.CGColor;
  self.layer4.borderWidth = borderWidth;
  self.layer4.zPosition = -1 * (sinf(DegreesToRadians(layer0Angle))*radius);
  self.layer4.transform = CATransform3DMakeRotation(DegreesToRadians(90 - startingAngle), 0.0f, -1.0f, 0.0f);
  
  CGFloat leftMargin = -420.0f;
  CGPoint position = CGPointMake((leftMargin + (layerWidth/2.0f)), [UIScreen mainScreen].bounds.size.width/2.0f);
  
  CGFloat fistLayerTrigLength = cosf(DegreesToRadians(startingAngle)) * radius;
  CGFloat secondLayerTrigLength = cosf(DegreesToRadians(startingAngle + angleStep)) * radius;
  
  self.layer0.position = position;
  position.x += (fistLayerTrigLength - secondLayerTrigLength);
  self.layer1.position = position;
  position.x += secondLayerTrigLength;
  self.layer2.position = position;
  position.x += secondLayerTrigLength;
  self.layer3.position = position;
  position.x += (fistLayerTrigLength - secondLayerTrigLength);
  self.layer4.position = position;
  
  // Add sublayers.
  [self.view.layer addSublayer:self.layer0];
  [self.view.layer addSublayer:self.layer1];
  [self.view.layer addSublayer:self.layer2];
  [self.view.layer addSublayer:self.layer3];
  [self.view.layer addSublayer:self.layer4];
  
  
  self.testLayer = [CALayer layer];
  self.testLayer.position = CGPointMake(100.0f, 100.0f);
  self.testLayer.bounds = CGRectMake(0.0f, 0.0, 100.0f, 100.0f);
  self.testLayer.backgroundColor = [UIColor whiteColor].CGColor;
  [self.view.layer addSublayer:self.testLayer];
  
  UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
  [self.view addGestureRecognizer:tapGestureRecognizer];
}

- (void)handleTap:(UITapGestureRecognizer*)tapGestureRecognizer {
  [CATransaction setDisableActions:YES];
  
  /*
  self.testLayer.position = CGPointMake(1000.0f, 100.0f);
  
  CAKeyframeAnimation *keyframeAnimation =
      [self keyFrameAnimationForKey:@"position"
                          fromValue:[NSValue valueWithCGPoint:CGPointMake(100.0f, 100.0f)]
                            toValue:[NSValue valueWithCGPoint:CGPointMake(1000.0f, 100.0f)]];
  [self.testLayer addAnimation:keyframeAnimation forKey:@""];
  */
  
  
  
  CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
  transformAnimation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
  transformAnimation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeTranslation(0.0f, 0.0f, -1.0f * radius)];
  transformAnimation.duration = 10;
  [self.layer2 addAnimation:transformAnimation forKey:@""];
  
  
  
  
  
  
  /*
  CGPoint fromPosition = self.layer4.position;
  CGFloat fromZPosition = self.layer4.zPosition;
  CATransform3D fromTransform = self.layer4.transform;
  
  self.layer4.position = self.layer3.position;
  self.layer4.zPosition = self.layer3.zPosition;
  self.layer4.transform = self.layer3.transform;
  
  
  CGFloat xStep = (fromPosition.x - self.layer4.position.x)/4.0f;
  CGFloat xPosition = fromPosition.x;
  CGFloat zPosition = fromZPosition;
  
  NSMutableArray *positions = [NSMutableArray array];
  NSMutableArray *zPositions = [NSMutableArray array];
  
  [positions addObject:[NSValue valueWithCGPoint:CGPointMake(fromPosition.x, fromPosition.y)]];
  [zPositions addObject:@(zPosition)];
  
  xPosition = xPosition - xStep;
  [positions addObject:[NSValue valueWithCGPoint:CGPointMake(xPosition, fromPosition.y)]];
  [zPositions addObject:@(-1 * sqrtf((radius*radius) - ((xPosition-509.365)*(xPosition-509.365))))];
  
  xPosition = xPosition - xStep;
  [positions addObject:[NSValue valueWithCGPoint:CGPointMake(xPosition, fromPosition.y)]];
  [zPositions addObject:@(-1 * sqrtf((radius*radius) - ((xPosition-509.365)*(xPosition-509.365))))];
  
  xPosition = xPosition - xStep;
  [positions addObject:[NSValue valueWithCGPoint:CGPointMake(xPosition, fromPosition.y)]];
  [zPositions addObject:@(-1 * sqrtf((radius*radius) - ((xPosition-509.365)*(xPosition-509.365))))];
  
  xPosition = xPosition - xStep;
  [positions addObject:[NSValue valueWithCGPoint:CGPointMake(xPosition, fromPosition.y)]];
  [zPositions addObject:@(-1 * sqrtf((radius*radius) - ((xPosition-509.365)*(xPosition-509.365))))];
  
  CAKeyframeAnimation *layer4PositionAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
  layer4PositionAnimation.values = positions;
  layer4PositionAnimation.keyTimes = @[@0.0f,@0.25f,@0.5f,@0.75f,@1.0f];
  layer4PositionAnimation.duration = 10;
  layer4PositionAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
  
  CAKeyframeAnimation *layer4ZPositionAnimation =  [CAKeyframeAnimation animationWithKeyPath:@"zPosition"];
  layer4ZPositionAnimation.values = zPositions;
  layer4ZPositionAnimation.keyTimes = @[@0.0f,@0.25f,@0.5f,@0.75f,@1.0f];
  layer4ZPositionAnimation.duration = 10;
  layer4ZPositionAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
  
  [self.layer4 addAnimation:layer4PositionAnimation forKey:@""];
  [self.layer4 addAnimation:layer4ZPositionAnimation forKey:@""];
  [self.layer4 addAnimation:
   [self animationForKey:@"transform" fromValue:[NSValue valueWithCATransform3D:fromTransform] toValue:[NSValue valueWithCATransform3D:self.layer4.transform]] forKey:@""];
  
  
  fromPosition = self.layer3.position;
  fromZPosition = self.layer3.zPosition;
  fromTransform = self.layer3.transform;
  
  self.layer3.position = self.layer2.position;
  self.layer3.zPosition = self.layer2.zPosition;
  self.layer3.transform = self.layer2.transform;
  
  xStep = (fromPosition.x - self.layer3.position.x)/4.0f;
  xPosition = fromPosition.x;
  zPosition = fromZPosition;
  
  positions = [NSMutableArray array];
  zPositions = [NSMutableArray array];
  
  [positions addObject:[NSValue valueWithCGPoint:CGPointMake(fromPosition.x, fromPosition.y)]];
  [zPositions addObject:@(zPosition)];
  
  xPosition = xPosition - xStep;
  [positions addObject:[NSValue valueWithCGPoint:CGPointMake(xPosition, fromPosition.y)]];
  [zPositions addObject:@(-1 * sqrtf((radius*radius) - ((xPosition-509.365)*(xPosition-509.365))))];
  
  xPosition = xPosition - xStep;
  [positions addObject:[NSValue valueWithCGPoint:CGPointMake(xPosition, fromPosition.y)]];
  [zPositions addObject:@(-1 * sqrtf((radius*radius) - ((xPosition-509.365)*(xPosition-509.365))))];
  
  xPosition = xPosition - xStep;
  [positions addObject:[NSValue valueWithCGPoint:CGPointMake(xPosition, fromPosition.y)]];
  [zPositions addObject:@(-1 * sqrtf((radius*radius) - ((xPosition-509.365)*(xPosition-509.365))))];
  
  xPosition = xPosition - xStep;
  [positions addObject:[NSValue valueWithCGPoint:CGPointMake(xPosition, fromPosition.y)]];
  [zPositions addObject:@(-1 * sqrtf((radius*radius) - ((xPosition-509.365)*(xPosition-509.365))))];
  
  CAKeyframeAnimation *layer3PositionAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
  layer3PositionAnimation.values = positions;
  layer3PositionAnimation.keyTimes = @[@0.0f,@0.25f,@0.5f,@0.75f,@1.0f];
  layer3PositionAnimation.duration = 10;
  layer3PositionAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
  
  CAKeyframeAnimation *layer3ZPositionAnimation =  [CAKeyframeAnimation animationWithKeyPath:@"zPosition"];
  layer3ZPositionAnimation.values = zPositions;
  layer3ZPositionAnimation.keyTimes = @[@0.0f,@0.25f,@0.5f,@0.75f,@1.0f];
  layer3ZPositionAnimation.duration = 10;
  layer3ZPositionAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
  
  [self.layer3 addAnimation:layer3PositionAnimation forKey:@""];
  [self.layer3 addAnimation:layer3ZPositionAnimation forKey:@""];
  
  [self.layer3 addAnimation:
   [self animationForKey:@"transform" fromValue:[NSValue valueWithCATransform3D:fromTransform] toValue:[NSValue valueWithCATransform3D:self.layer3.transform]] forKey:@""];
  */
  
  
  
  
  
  
  
  
  
  
  
  /*
//  [CATransaction setAnimationDuration:10];
  
  CGPoint fromPosition = self.layer4.position;
  CGFloat fromZPosition = self.layer4.zPosition;
  CATransform3D fromTransform = self.layer4.transform;
  
  self.layer4.position = self.layer3.position;
  self.layer4.zPosition = self.layer3.zPosition;
  self.layer4.transform = self.layer3.transform;
  
  [self.layer4 addAnimation:
   [self keyFrameAnimationForKey:@"position"
               fromValue:[NSValue valueWithCGPoint:fromPosition]
                 toValue:[NSValue valueWithCGPoint:self.layer4.position]] forKey:@""];
  [self.layer4 addAnimation:
   [self keyFrameAnimationForKey:@"zPosition"
               fromValue:@(fromZPosition)
                 toValue:@(self.layer4.zPosition)] forKey:@""];
  [self.layer4 addAnimation:
   [self animationForKey:@"transform" fromValue:[NSValue valueWithCATransform3D:fromTransform] toValue:[NSValue valueWithCATransform3D:self.layer4.transform]] forKey:@""];
  
  
  fromPosition = self.layer3.position;
  fromZPosition = self.layer3.zPosition;
  fromTransform = self.layer3.transform;
  
  self.layer3.position = self.layer2.position;
  self.layer3.zPosition = self.layer2.zPosition;
  self.layer3.transform = self.layer2.transform;
  
  [self.layer3 addAnimation:
   [self keyFrameAnimationForKey:@"position"
               fromValue:[NSValue valueWithCGPoint:fromPosition]
                 toValue:[NSValue valueWithCGPoint:self.layer3.position]] forKey:@""];
  [self.layer3 addAnimation:
   [self keyFrameAnimationForKey:@"zPosition"
               fromValue:@(fromZPosition)
                 toValue:@(self.layer3.zPosition)] forKey:@""];
  [self.layer3 addAnimation:
   [self animationForKey:@"transform" fromValue:[NSValue valueWithCATransform3D:fromTransform] toValue:[NSValue valueWithCATransform3D:self.layer3.transform]] forKey:@""];
  
  
  self.layer2.position = self.layer1.position;
  self.layer2.zPosition = self.layer1.zPosition;
  self.layer2.transform = self.layer1.transform;
  
  self.layer1.position = self.layer0.position;
  self.layer1.zPosition = self.layer0.zPosition;
  self.layer1.transform = self.layer0.transform;
   */
}

- (CABasicAnimation*)animationForKey:(NSString*)key fromValue:(id)fromValue toValue:(id)toValue {
  CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:key];
  animation.fromValue = fromValue;
  animation.toValue = toValue;
  animation.duration = 10.0;
  animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
  return animation;
}

- (CAKeyframeAnimation*)keyFrameAnimationForKey:(NSString*)key
                                      fromValue:(id)fromValue
                                        toValue:(id)toValue {
  CAKeyframeAnimation *keyFrameAnimation = [CAKeyframeAnimation animationWithKeyPath:key];
  keyFrameAnimation.values = @[fromValue, [NSValue valueWithCGPoint:CGPointMake(500.0f, 500.0f)], toValue];
  keyFrameAnimation.keyTimes = @[@0.0f, @0.5f, @1.0f];
  keyFrameAnimation.calculationMode = kCAAnimationCubic;
  keyFrameAnimation.duration = 10.0f;
  keyFrameAnimation.tensionValues = @[@(-1.0f)];
  return keyFrameAnimation;
}

@end
