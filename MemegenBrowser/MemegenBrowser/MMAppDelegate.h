@class MMMemeSlideshowViewController;

@interface MMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MMMemeSlideshowViewController *viewController;

@end
