#import "MMMemeCollectionViewCell.h"

@interface MMMemeCollectionViewCell ()
@property(nonatomic, strong) UIImageView *memeImageView;
@property(nonatomic, strong) UILabel *authorLabel;
@end

@implementation MMMemeCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
      self.layer.backgroundColor = [UIColor whiteColor].CGColor;
      self.layer.borderColor = [UIColor whiteColor].CGColor;
      self.layer.borderWidth = 6.0f;
      self.layer.shadowColor = [UIColor blackColor].CGColor;
      self.layer.shadowRadius = 5.0f;
      self.layer.shadowOffset = CGSizeMake(0.0f, 6.0f);
      self.layer.shadowOpacity = 0.8;
      self.layer.cornerRadius = 2.0f;
      
      _memeImageView = [self newImageView];
      _authorLabel = [self newLabel];
      _authorLabel.textAlignment = NSTextAlignmentCenter;
      
      [self constructViewHierarchy];
    }
    return self;
}

- (void)layoutSubviews {
  [super layoutSubviews];
  
  CGRect imageViewFrame = self.bounds;
  imageViewFrame.size.height -= 60.0f;
  self.memeImageView.frame = imageViewFrame;
  
  CGRect authorLabelFrame = self.bounds;
  authorLabelFrame.size.height = 60.0f;
  authorLabelFrame = CGRectOffset(authorLabelFrame, 0.0f, self.bounds.size.height - 60.0f);
  authorLabelFrame = CGRectInset(authorLabelFrame, 6.0f, 0.0f);
  self.authorLabel.frame = authorLabelFrame;
  // TODO: Add bend to shadow with shadow path.
  self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
}

- (void)setImage:(UIImage *)image {
  self.memeImageView.image = image;
  CGRect imageViewFrame = self.bounds;
  imageViewFrame.size.height -= 60.0f;
  self.memeImageView.frame = imageViewFrame;
}

- (void)setAuthor:(NSString *)author {
  self.authorLabel.text = author;
}

- (UIImageView *)newImageView {
  UIImageView *imageView = [[UIImageView alloc] init];
  imageView.contentMode = UIViewContentModeScaleAspectFill;
  imageView.clipsToBounds = YES;
  return imageView;
}

- (UILabel *)newLabel {
  CGRect frame = self.bounds;
  frame.size.height = 60.0f;
  frame = CGRectOffset(frame, 0.0f, self.bounds.size.height - 60.0f);
  frame = CGRectInset(frame, 6.0f, 0.0f);
  UILabel *label = [[UILabel alloc] initWithFrame:self.bounds];
  label.font = [UIFont fontWithName:@"Heiti SC" size:28.0f];
  label.backgroundColor = [UIColor whiteColor];
  label.adjustsFontSizeToFitWidth = YES;
  
  return label;
}

- (void)constructViewHierarchy {
  [self addSubview:self.memeImageView];
  [self addSubview:self.authorLabel];
}

@end
