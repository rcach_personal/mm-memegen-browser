#import "MM3DGridViewController.h"

#import "MM3DGridCell.h"
#import "MM3DGridLayout.h"

@interface MM3DGridViewController ()<UICollectionViewDataSource>
@property(nonatomic, strong) UICollectionView *collectionView;
@property(nonatomic, strong) NSMutableArray *memes;
@end

@implementation MM3DGridViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadView {
  self.view = [[UIView alloc] init];
  self.view.backgroundColor = [UIColor blackColor];
  self.view.autoresizesSubviews = YES;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.memes = [self newMemes];
  self.collectionView = [self newCollectionView];
  [self constructViewHierarchy];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  self.collectionView.frame = CGRectOffset(self.view.bounds, 400.0f, 0.0f);
  
  CGFloat itemWidth = (self.view.bounds.size.width / 5.0f) - 20.0f;
  ((UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout).itemSize = CGSizeMake(itemWidth, 100.0f);
  
  self.collectionView.layer.anchorPoint = CGPointMake(1.0f, 0.5f);
  CATransform3D perspective = CATransform3DIdentity;
  perspective.m34 = 1.0f / -1000.0f;
  self.view.layer.sublayerTransform = perspective;
  self.collectionView.layer.transform = CATransform3DMakeRotation(-40 * M_PI / 180, 0.0f, 1.0f, 0.0f);
  self.collectionView.clipsToBounds = NO;
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
  rotationAnimation.fromValue =
      [NSValue valueWithCATransform3D:CATransform3DMakeRotation(-150 * M_PI / 180, 0.0f, 1.0f, 0.0f)];
  rotationAnimation.toValue =
      [NSValue valueWithCATransform3D:CATransform3DMakeRotation(-40 * M_PI / 180, 0.0f, 1.0f, 0.0f)];
  rotationAnimation.duration = 10.0;
  rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
  [self.collectionView.layer addAnimation:rotationAnimation forKey:@""];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)constructViewHierarchy {
  [self.view addSubview:self.collectionView];
}

- (UICollectionView *)newCollectionView {
  UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
  flowLayout.itemSize = CGSizeMake(100.0f, 100.0f);
  flowLayout.minimumLineSpacing = 40.0f;
  flowLayout.sectionInset = UIEdgeInsetsMake(20.0f, 20.0f, 20.0f, 20.0f);
  UICollectionView *collectionView =
      [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
  [collectionView registerClass:[MM3DGridCell class] forCellWithReuseIdentifier:@"3DCell"];
  collectionView.dataSource = self;
  collectionView.autoresizingMask = (UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
  return collectionView;
}

- (NSMutableArray *)newMemes {
  NSMutableArray *memes = [NSMutableArray array];
  for (NSUInteger memeIndex = 0; memeIndex < 35; memeIndex++) {
    [memes addObject:[NSNull null]];
  }
  return memes;
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return [self.memes count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  MM3DGridCell *cell =
      (MM3DGridCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"3DCell" forIndexPath:indexPath];
  return cell;
}

@end
