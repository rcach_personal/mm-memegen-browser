#import "MMParse.h"

#import <Parse/Parse.h>

@implementation MMParse

static MMParse *sharedInstance;

+ (MMParse *)sharedInstance {
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedInstance = [[self alloc] init];
  });
  return sharedInstance;
}

- (void)setUp {
  [Parse setApplicationId:@"JLeugHIeavTnEoGoU5X6bDeBDZhjn0NUTFLGyS5t"
                clientKey:@"ZBqgdsuoQM159hTpseoM1SjuPZMvUkNF0mFQDPzL"];
  
  // Wipe out old user defaults
  if ([[NSUserDefaults standardUserDefaults] objectForKey:@"objectIDArray"]){
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"objectIDArray"];
  }
  [[NSUserDefaults standardUserDefaults] synchronize];
  
  // Simple way to create a user or log in the existing user
  // For your app, you will probably want to present your own login screen
  PFUser *currentUser = [PFUser currentUser];
  
  if (!currentUser) {
    [PFUser logInWithUsername:@"RCach" password:@"password"];
  }
}

@end
