#import "MMCollectionViewSinglePhotoStackLayout.h"

static NSString * const kPhotoStackLayoutPhotoItemKind = @"PhotoItem";

@interface MMCollectionViewSinglePhotoStackLayout ()
@property(nonatomic, strong) NSMutableArray *rotations;
@property(nonatomic, strong) NSMutableSet *deletionIndexPaths;
@property(nonatomic, strong) NSMutableSet *insertionsIndexPaths;
@property(nonatomic, assign) NSUInteger rotationOffset;
@property(nonatomic, assign) NSUInteger zIndexOffset;
@end

@implementation MMCollectionViewSinglePhotoStackLayout

- (id)init {
  self = [super init];
  if (self) {
    self.rotations = [self newRotations];
    self.deletionIndexPaths = [NSMutableSet set];
    self.insertionsIndexPaths = [NSMutableSet set];
    self.rotationOffset = [self.rotations count] - 1;
    self.zIndexOffset = 0;
  }
  return self;
}

- (CGSize)collectionViewContentSize {
  return self.collectionView.bounds.size;
}

- (NSArray*)layoutAttributesForElementsInRect:(CGRect)rect {
  NSMutableArray *itemAttributesInRect = [NSMutableArray array];
//  NSLog(@"LAYOUT");
  for (NSUInteger itemIndex = 0; itemIndex < [self.collectionView numberOfItemsInSection:0]; itemIndex++) {
    NSIndexPath* indexPath = [NSIndexPath indexPathForItem:itemIndex inSection:0];
    [itemAttributesInRect addObject:[self layoutAttributesForItemAtIndexPath:indexPath]];
  }
//  NSLog(@"----");
  return itemAttributesInRect;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
  UICollectionViewLayoutAttributes *itemAttributes =
      [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
  itemAttributes.frame = [self itemFrameForItemAtIndexPath:indexPath];
  itemAttributes.zIndex = -indexPath.item - self.zIndexOffset;
  itemAttributes.transform3D = [self.rotations[indexPath.item + self.rotationOffset] CATransform3DValue];
  itemAttributes.center = CGPointMake(CGRectGetMidX(self.collectionView.bounds),
                                      CGRectGetMidY(self.collectionView.bounds) + 40.0f);
  
//  NSLog(@"Item %@ zIndex: %i", indexPath, itemAttributes.zIndex);
  return itemAttributes;
}

- (void)prepareForCollectionViewUpdates:(NSArray *)updateItems {
  for (UICollectionViewUpdateItem *updateItem in updateItems) {
    if (updateItem.updateAction == UICollectionUpdateActionDelete) {
      [self.deletionIndexPaths addObject:updateItem.indexPathBeforeUpdate];
    } else if (updateItem.updateAction == UICollectionUpdateActionInsert) {
      if (![self.insertionsIndexPaths containsObject:updateItem.indexPathBeforeUpdate]) {
        if (self.rotationOffset == 0) {
          self.rotationOffset = [self.rotations count] - 1;
        }
        self.zIndexOffset++;
        self.rotationOffset--;
      }
      [self.insertionsIndexPaths addObject:updateItem.indexPathAfterUpdate];
    }
  }
  [super prepareForCollectionViewUpdates:updateItems];
}

- (UICollectionViewLayoutAttributes *)initialLayoutAttributesForAppearingItemAtIndexPath:(NSIndexPath *)itemIndexPath {
//  NSLog(@"Initial Layout: %@", itemIndexPath);
  UICollectionViewLayoutAttributes *initialLayoutAttributes =
      [super initialLayoutAttributesForAppearingItemAtIndexPath:itemIndexPath];
  
  if ([self.insertionsIndexPaths containsObject:itemIndexPath]) {
    if (!initialLayoutAttributes) {
      initialLayoutAttributes = [self.collectionView layoutAttributesForItemAtIndexPath:itemIndexPath];
    }
//    initialLayoutAttributes.alpha = 0.0f;
//    initialLayoutAttributes.center = CGPointZero;
  }
  
  return initialLayoutAttributes;
}

- (UICollectionViewLayoutAttributes *)finalLayoutAttributesForDisappearingItemAtIndexPath:(NSIndexPath *)itemIndexPath {
//  NSLog(@"Final Layout: %@", itemIndexPath);
  UICollectionViewLayoutAttributes *finalLayoutAttributes =
      [super finalLayoutAttributesForDisappearingItemAtIndexPath:itemIndexPath];
  
//  if ([self.deletionIndexPaths containsObject:itemIndexPath]) {
//    if (!finalLayoutAttributes) {
//      finalLayoutAttributes =
//          [self.collectionView layoutAttributesForItemAtIndexPath:itemIndexPath];
//    }
//    finalLayoutAttributes.alpha = 0.0f;
//    finalLayoutAttributes.center = CGPointMake(CGRectGetMaxX(self.collectionView.bounds) * 1.5f,
//                                               CGRectGetMaxY(self.collectionView.bounds) * 1.5f);
//  }
  return finalLayoutAttributes;
}

- (void)finalizeCollectionViewUpdates {
  
  NSIndexPath *insertedCellIndexPath = [self.insertionsIndexPaths anyObject];
  UICollectionViewCell *insertedCell = [self.collectionView cellForItemAtIndexPath:insertedCellIndexPath];
  
  
  int randomNumber = arc4random() % 10;
  
  
  CAKeyframeAnimation *positionAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
  UIBezierPath *positionPath = [UIBezierPath bezierPath];
  CGPoint startPoint;
  CGPoint endPoint = endPoint = CGPointMake(CGRectGetMidX(self.collectionView.bounds),
                                            CGRectGetMidY(self.collectionView.bounds) + 40.0f);
  
  if (randomNumber < 5) {
    startPoint = CGPointMake(CGRectGetMaxX(self.collectionView.bounds), CGRectGetMidY(self.collectionView.bounds));
    
  } else {
    startPoint = CGPointMake(CGRectGetMinX(self.collectionView.bounds), CGRectGetMidY(self.collectionView.bounds));
  }
  
  
  [positionPath moveToPoint:startPoint];
  [positionPath addLineToPoint:endPoint];
  positionAnimation.path = positionPath.CGPath;
  
  CABasicAnimation *opacityAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
  opacityAnimation.fromValue = @(0.0f);
  opacityAnimation.toValue = @(1.0f);
  
  CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
  rotationAnimation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
  rotationAnimation.toValue = self.rotations[insertedCellIndexPath.item + self.rotationOffset];
  
  CAAnimationGroup *imageInsertAnimation = [CAAnimationGroup animation];
  imageInsertAnimation.animations = @[positionAnimation, opacityAnimation, rotationAnimation];
  imageInsertAnimation.duration = 1.5;
  imageInsertAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
  [insertedCell.layer addAnimation:imageInsertAnimation forKey:@""];
  
  [self.deletionIndexPaths removeAllObjects];
  [self.insertionsIndexPaths removeAllObjects];
  
  [super finalizeCollectionViewUpdates];
}

- (CGRect)itemFrameForItemAtIndexPath:(NSIndexPath*)indexPath {
  CGSize size = CGSizeMake(500.0f, 680.0f);
  id<UICollectionViewDelegateMMSinglePhotoStackLayout> delegate =
      (id<UICollectionViewDelegateMMSinglePhotoStackLayout>)self.collectionView.delegate;
  if (delegate) {
    if ([delegate respondsToSelector:@selector(collectionView:layout:sizeForItemAtIndexPath:)]) {
      size = [delegate collectionView:self.collectionView layout:self sizeForItemAtIndexPath:indexPath];
    }
  }
  return CGRectMake(0.0f, 0.0f, size.width, size.height);
}

- (NSMutableArray *)newRotations {
  NSMutableArray *rotations = [NSMutableArray array];
  CGFloat percentage = 0.0f;
  for (NSInteger rotationIndex = 0; rotationIndex < 5000; rotationIndex++) {
    CGFloat newPercentage = 0.0f;
    do {
      newPercentage = ((CGFloat)(arc4random() % 220) - 110) * 0.0001f;
    } while (fabsf(percentage - newPercentage) < 0.008);
    percentage = newPercentage;
//    NSLog(@"Percentage Angle: %f", percentage);
    CGFloat angle = 2 * M_PI * (1.0f + percentage);
//    NSLog(@"Rotation Angle: %f", (angle/(2.0f * M_PI)) - 1.0f);
    CATransform3D transform = CATransform3DMakeRotation(angle, 0.0f, 0.0f, 1.0f);
    [rotations addObject:[NSValue valueWithCATransform3D:transform]];
  }
  return rotations;
}




@end
