#import "MM3DGridLayout.h"

// TODO: Add perspective here instead of the view controller.
@implementation MM3DGridLayout

- (void)prepareLayout {
  [super prepareLayout];
  CATransform3D perspective = CATransform3DIdentity;
  perspective.m34 = 1.0f / -800.0f;
//  self.collectionView.layer.sublayerTransform = perspective;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
  NSArray *layoutAttributes = [super layoutAttributesForElementsInRect:rect];
  
  for (UICollectionViewLayoutAttributes *itemAttributes in layoutAttributes) {
    itemAttributes.center = CGPointMake(500.0f, itemAttributes.center.y);
    itemAttributes.transform3D = [self rotationTransformForIndexPath:itemAttributes.indexPath];
  }
  return layoutAttributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
  UICollectionViewLayoutAttributes *itemAttributes = [super layoutAttributesForItemAtIndexPath:indexPath];
  itemAttributes.center = CGPointMake(500.0f, itemAttributes.center.y);
  itemAttributes.transform3D = [self rotationTransformForIndexPath:indexPath];
  return itemAttributes;
}

- (CATransform3D)rotationTransformForIndexPath:(NSIndexPath *)indexPath {
  CATransform3D rotationTransform = CATransform3DMakeRotation(0 * M_PI / 180, 0.0f, 0.0f, 0.0f);
  
  
  
  CGFloat factor = fmodf(indexPath.item, 5.0f);
  
//  NSLog(@"Item %i column: %f", indexPath.item, factor);
  
  rotationTransform = CATransform3DTranslate(rotationTransform, (10 * factor) + (factor * 0.0f), 0.0f, factor);
  
  return rotationTransform;
}

@end
