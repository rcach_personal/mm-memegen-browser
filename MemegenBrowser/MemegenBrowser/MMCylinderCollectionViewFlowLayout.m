#import "MMCylinderCollectionViewFlowLayout.h"

@implementation MMCylinderCollectionViewFlowLayout

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
  return YES;
}

- (UICollectionViewLayoutAttributes*)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
  UICollectionViewLayoutAttributes *attributes = [super layoutAttributesForItemAtIndexPath:indexPath];
  return attributes;
}

- (NSArray*)layoutAttributesForElementsInRect:(CGRect)rect {
  NSArray *allAttributesInRect = [super layoutAttributesForElementsInRect:rect];
  CGRect visibleRect;
  visibleRect.origin = self.collectionView.contentOffset;
  visibleRect.size = self.collectionView.bounds.size;
  
  for (UICollectionViewLayoutAttributes *cellAttributes in allAttributesInRect) {
    if (CGRectIntersectsRect(cellAttributes.frame, rect)) {
      CGFloat distance = CGRectGetMidX(visibleRect) - cellAttributes.center.x;
      
      CGFloat count = 12.0f;
      CGFloat spacing = 0.9f;
      CGFloat arc = M_PI * 2.0f;
      CGFloat radius = fmaxf(cellAttributes.size.width * spacing / 2.0f, cellAttributes.size.width * spacing / 2.0f / tanf(arc/2.0f/count));
      CGFloat angle = (distance/cellAttributes.size.width) / count * arc;
      
      cellAttributes.center = CGPointMake(CGRectGetMidX(visibleRect), cellAttributes.center.y);
      
      CATransform3D transform = CATransform3DIdentity;
      transform.m34 = 1.0f/-700.0f;
      transform = CATransform3DTranslate(transform, 0.0f, 0.0f, radius);
      transform = CATransform3DRotate(transform, angle, 0.0f, 1.0f, 0.0f);
      cellAttributes.transform3D = CATransform3DTranslate(transform, 0.0f, 0.0f, -radius + 1.00f);
      
      
    }
  }
  
  return allAttributesInRect;
}

@end
