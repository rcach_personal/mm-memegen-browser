@interface MMExternalDisplay : NSObject

typedef UIViewController *(^UIViewControllerFactory)(void);

+ (void)setUpWithViewControllerFactory:(UIViewControllerFactory)viewControllerFactory;

@end
