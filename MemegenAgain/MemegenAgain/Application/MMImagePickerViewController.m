//
//  MMImagePickerViewController.m
//  MemegenAgain
//
//  Created by Rene Cacheaux on 1/15/13.
//  Copyright (c) 2013 Rene Cacheaux. All rights reserved.
//

#import "MMImagePickerViewController.h"

@interface MMImagePickerViewController ()
@end

@implementation MMImagePickerViewController

- (NSInteger)collectionView:(UICollectionView*)collectionView
     numberOfItemsInSection:(NSInteger)section {
  return [MMMemeImages numberOfImages];
}

- (UICollectionViewCell*)collectionView:(UICollectionView*)collectionView
                 cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  MMImageCell *imageCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"memeCell" forIndexPath:indexPath];
  UIImage *memeImage = [MMMemeImages imageAtIndexPath:indexPath];
  imageCell.imageView.image = memeImage;
  return imageCell;
}

- (CGSize)collectionView:(UICollectionView*)collectionView
                          layout:(UICollectionViewLayout *)collectionViewLayout
          sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  return [MMMemeImages smallSizeForImageAtIndexPath:indexPath];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  MMImageCell *imageCell = [collectionView cellForItemAtIndexPath:indexPath];
  UIImage *memeImage = imageCell.imageView.image;
  [self performSegueWithIdentifier:@"textEntrySegue" sender:memeImage];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  MMTextEntryViewController *textEntryViewController = segue.destinationViewController;
  textEntryViewController.memeImage = sender;
}

@end
