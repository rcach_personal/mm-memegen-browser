//
//  MMTextEntryViewController.m
//  MemegenAgain
//
//  Created by Rene Cacheaux on 1/15/13.
//  Copyright (c) 2013 Rene Cacheaux. All rights reserved.
//

#import "MMTextEntryViewController.h"

#import "MMTextPlacementViewController.h"

@interface MMTextEntryViewController ()
@property(nonatomic, weak) IBOutlet UIImageView *memeImageView;
@property(nonatomic, weak) IBOutlet UITextView *memeTextView;
@end

@implementation MMTextEntryViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.memeImageView.image = self.memeImage;
}

- (void)viewWillAppear:(BOOL)animated {
  [self.memeTextView becomeFirstResponder];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  // TODO: Implement this method.
}

@end
