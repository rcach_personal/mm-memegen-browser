//
//  MMTextPlacementViewController.h
//  MemegenAgain
//
//  Created by Rene Cacheaux on 1/15/13.
//  Copyright (c) 2013 Rene Cacheaux. All rights reserved.
//

@interface MMTextPlacementViewController : UIViewController
@property(nonatomic, strong) UIImage *memeImage;
@property(nonatomic, strong) NSString *memeText;
@property (weak, nonatomic) IBOutlet UILabel *memeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *memeLabelHeightConstraint;
@end
