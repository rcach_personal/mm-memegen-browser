//
//  MMTextPlacementViewController.m
//  MemegenAgain
//
//  Created by Rene Cacheaux on 1/15/13.
//  Copyright (c) 2013 Rene Cacheaux. All rights reserved.
//

#import "MMTextPlacementViewController.h"

@interface MMTextPlacementViewController ()
@property (weak, nonatomic) IBOutlet UIView *memeView;
@property (weak, nonatomic) IBOutlet UIImageView *memeImageView;
@property (weak, nonatomic) IBOutlet UIView *savingView;
@property (nonatomic, strong) MMTextPlacementViewControllerConstraintManager *constraintManager;
@end

@implementation MMTextPlacementViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  // TODO: Implement this method.
}

- (void)viewWillAppear:(BOOL)animated {
 // TODO: Implement this method.
}

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];
  // TODO: Implement this method.
}

- (IBAction)share:(id)sender {
  // TODO: Implement this method.
}

@end
