//
//  MMAppDelegate.m
//  MemegenAgain
//
//  Created by Rene Cacheaux on 1/15/13.
//  Copyright (c) 2013 Rene Cacheaux. All rights reserved.
//

#import "MMAppDelegate.h"

@implementation MMAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    return YES;
}
							
@end
