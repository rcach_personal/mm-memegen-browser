//
//  UIImageView+MMContentScale.h
//  MMMemegen
//
//  Created by Rene Cacheaux on 11/14/12.
//  Copyright (c) 2012 Mutual Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (MMContentScale)

-(CGFloat)contentScaleFactor;

@end
